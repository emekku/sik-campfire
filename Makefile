MKDIR		=mkdir -p
RM			=-rm -fv
RM_R		=-rm -rfv

BUILD_DIR	=build

.PHONY: all clean

all:
	$(MKDIR) $(BUILD_DIR); \
	cd $(BUILD_DIR); \
	cmake .. && make; \
	echo Run using $(BUILD_DIR)/bin/server or $(BUILD_DIR)/bin/client

clean:
	$(RM_R) $(BUILD_DIR)
	$(RM) *.o
