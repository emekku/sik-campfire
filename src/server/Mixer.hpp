/*******************************************************************************
*                                                                              *
* server/Mixer.hpp                                                             *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#ifndef MIXER_HPP
#define MIXER_HPP

#include <cstdlib>

struct mixer_input {
    void *data;
    size_t len;
    size_t consumed;
};

void mixer(struct mixer_input *inputs,
           size_t n,
           void *output_buf,
           size_t *output_size,
           unsigned long tx_interval_ms);

#endif // MIXER_HPP
