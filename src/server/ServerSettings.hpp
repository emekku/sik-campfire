/*******************************************************************************
*                                                                              *
* server/ServerSettings.hpp                                                    *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#ifndef SERVER_SERVERSETTINGS_HPP
#define SERVER_SERVERSETTINGS_HPP

class ServerSettings
{
public:
    uint16_t port = 14685;
    size_t bufLen = 10;
    size_t fifoSize = 10560;
    size_t fifoHighWatermark = fifoSize;
    size_t fifoLowWatermark = 0;
    unsigned long txInterval = 5;

    unsigned int dataLenFactor = 176;
    int reportFreq = 1000;
};

#endif // SERVER_SERVERSETTINGS_HPP
