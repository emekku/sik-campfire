/*******************************************************************************
*                                                                              *
* server/Server.cpp                                                            *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#include "Server.hpp"

#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <iostream>
#include <sstream>

Server::Server(const ServerSettings &settings)
    : settings(settings),
      serverTCP(ioService, &clients, settings),
      serverUDP(ioService, &clients, settings)
{
}

void Server::run()
{
    std::cout << "Server listening on port " << settings.port << ".\n";

    ioService.run();
}
