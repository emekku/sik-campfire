/*******************************************************************************
*                                                                              *
* server/ServerTCP.hpp                                                         *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#ifndef SERVER_SERVERTCP_HPP
#define SERVER_SERVERTCP_HPP

#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>
#include "ServerClient.hpp"
#include "ServerSettings.hpp"

using boost::shared_ptr;
using boost::asio::ip::tcp;

class ServerTCP
{
public:
    ServerTCP(boost::asio::io_service &ioService,
              std::map<uint32_t, ServerClient::pointer> *clients,
              const ServerSettings &settings);


private:
    void handleAccept(ServerClient::pointer newConnection,
                      const boost::system::error_code &ec);
    void sendReports(shared_ptr<boost::asio::deadline_timer> dt,
                     const boost::system::error_code &ec);
    void startAccept();

    const ServerSettings &settings;

    uint32_t nextId = 1;
    std::map<uint32_t, ServerClient::pointer> *clients;

    tcp::acceptor acceptor;

};

#endif // SERVER_SERVERTCP_HPP
