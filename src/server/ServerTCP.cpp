/*******************************************************************************
*                                                                              *
* server/ServerTCP.cpp                                                         *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#include "ServerTCP.hpp"

#include <boost/bind.hpp>


ServerTCP::ServerTCP(boost::asio::io_service &ioService,
                     std::map<uint32_t, ServerClient::pointer> *clients,
                     const ServerSettings &settings)
    : settings(settings),
      clients(clients),
      acceptor(ioService, tcp::endpoint(tcp::v6(), settings.port))
{
    startAccept();

    // Start timer for connection reports.
    using boost::asio::deadline_timer;
    auto interval = boost::posix_time::millisec(settings.reportFreq);
    shared_ptr<deadline_timer> dt(
                new deadline_timer(acceptor.get_io_service(), interval));
    dt->async_wait(boost::bind(&ServerTCP::sendReports, this, dt,
                               boost::asio::placeholders::error));

}

void ServerTCP::handleAccept(ServerClient::pointer newClient,
                             const boost::system::error_code &ec)
{
    if (!ec) {
        clients->insert(std::make_pair(newClient->id(), newClient));
        newClient->start();

    } else {
        std::cerr << "Server TCP accept failed: " << ec.message()
                  << "\n";
    }

    startAccept();

}

void ServerTCP::sendReports(shared_ptr<boost::asio::deadline_timer> dt,
                           const boost::system::error_code &ec)
{
    dt->expires_at(dt->expires_at() +
                   boost::posix_time::millisec(settings.reportFreq));
    dt->async_wait(boost::bind(&ServerTCP::sendReports, this, dt,
                               boost::asio::placeholders::error));

    std::vector<uint32_t> brokenClients;

    if (clients->empty())
        return;

    if (ec)
        std::cerr << "ServerTCP reports timer failed: " << ec.message() << "\n";
    else {
        std::stringstream report;
        report << "\n";
        for (auto &it : *clients) {
            auto client = it.second;

            try {
                report << client->socket().remote_endpoint()
                       << " FIFO: " << client->fifoSize() << "/"
                       << settings.fifoSize
                       << " (min. " << client->fifoMin()
                       << ", max. " << client->fifoMax() << ")\n";
            } catch (std::exception /*&e*/) {
                client->connectionBroken(true);
                brokenClients.push_back(it.first);
            }

            client->resetFifoStats();
        }

        shared_ptr<std::string> reportMsg (new std::string(report.str()));

        for (auto &it : *clients) {
            auto client = it.second;
            tcp::socket &socket = client->socket();

            if (client->connectionBroken())
                continue;

            socket.async_write_some(boost::asio::buffer(*reportMsg),
                [this](boost::system::error_code ec, size_t /*bytes*/)
                {
                    if (ec)
                        std::cerr << "Server TCP reports send failed"
                                  << ec.message() << "\n";
                });
        }

        // Remove broken connections.
        for (auto client: brokenClients) {
            clients->erase(client);
        }
    }
}

void ServerTCP::startAccept()
{
    ServerClient::pointer newClient =
            ServerClient::create(nextId, settings, acceptor.get_io_service());
    nextId++;

    acceptor.async_accept(newClient->socket(),
        boost::bind(&ServerTCP::handleAccept, this, newClient,
            boost::asio::placeholders::error));
}
