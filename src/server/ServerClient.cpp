/*******************************************************************************
*                                                                              *
* server/ServerClient.cpp                                                      *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#include "ServerClient.hpp"

#include <algorithm>
#include <sstream>

ServerClient::pointer ServerClient::create(uint32_t id,
            const ServerSettings &settings, boost::asio::io_service &ioService)
{
    return pointer(new ServerClient(id, settings, ioService));
}

bool ServerClient::connectionBroken() const
{
    return connectionBroken_;
}

void ServerClient::connectionBroken(bool connectionBroken)
{
    connectionBroken_ = connectionBroken;
}

bool ServerClient::fifoActive() const
{
    return fifoState == FifoState::ACTIVE;
}

unsigned long ServerClient::id() const
{
    return clientId;
}

size_t ServerClient::fifoSize() const
{
    return fifoEnd - fifo.get();
}

size_t ServerClient::fifoMax() const
{
    return fifoMaxBytes;
}

size_t ServerClient::fifoMin() const
{
    return fifoMinBytes;
}

tcp::socket & ServerClient::socket()
{
    return clientSocket;
}

size_t ServerClient::win() const
{
    return settings.fifoSize - fifoSize();
}

unsigned long ServerClient::nextDatagramId()
{
    return nextDatagramId_;
}

void ServerClient::nextDatagramId(unsigned long newNextDatagramId)
{
    nextDatagramId_ = newNextDatagramId;
}

mixer_input ServerClient::mixerInput()
{
    return mixer_input { fifo.get(), fifoSize(), 0 };
}

bool ServerClient::ready() const
{
    return clientReady;
}

void ServerClient::ready(bool ready)
{
    clientReady = ready;
}

void ServerClient::resetFifoStats()
{
    fifoMaxBytes = fifoSize();
    fifoMinBytes = fifoSize();
}

void ServerClient::start()
{
    std::stringstream ss;
    ss << "CLIENT " << clientId << "\n";
    sendMessage = ss.str();

    clientSocket.async_write_some(boost::asio::buffer(sendMessage),
        [this](boost::system::error_code ec, size_t /*bytes*/)
        {
            if (ec) {
                std::cerr << "Server client connection send failed: "
                          << ec.message() << "\n";
            }
    });
}

void ServerClient::pushData(shared_ptr<std::string> data)
{
    size_t len = std::min(data->length(), settings.fifoSize);
    memcpy(fifoEnd, data->data(), len);
    fifoEnd += len;

    fifoMaxBytes = std::max(fifoMaxBytes, fifoSize());
    if (fifoSize() <= settings.fifoLowWatermark)
        fifoState = FifoState::FILLING;
    if (fifoSize() >= settings.fifoHighWatermark)
        fifoState = FifoState::ACTIVE;
}

void ServerClient::removeData(size_t len)
{
    size_t newSize = fifoSize() - len;
    for (size_t i = 0; i < newSize; ++i)
        fifo.get()[i] = fifo.get()[len + i];
    fifoEnd -= len;

    fifoMinBytes = std::min(fifoMinBytes, fifoSize());
    if (fifoSize() <= settings.fifoLowWatermark)
        fifoState = FifoState::FILLING;
    if (fifoSize() <= settings.fifoLowWatermark)
        fifoState = FifoState::ACTIVE;
}

ServerClient::ServerClient(uint32_t id, const ServerSettings &settings,
                           boost::asio::io_service &ioService)
    : clientId(id),
      clientSocket(ioService),
      settings(settings),
      fifo(new char[settings.fifoSize * 2])
{
    fifoEnd = fifo.get();
    fifoMinBytes = fifoSize();
    fifoState = FifoState::FILLING;
}
