/*******************************************************************************
*                                                                              *
* server/Mixer.cpp                                                             *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#include "Mixer.hpp"

#include <algorithm>
#include <cstring>

inline int16_t add(int16_t a, int16_t b)
{
    int32_t a32 = (int32_t) a, b32 = (int32_t) b;
    return (a32 + b32 > INT16_MAX) ?
                INT16_MAX : ((a32 + b32 < INT16_MIN) ? INT16_MIN : a32 + b32);
}

void mixer(struct mixer_input *inputs, size_t n, void *output_buf,
           size_t *output_size, unsigned long tx_interval_ms)
{
    int16_t *result_buf = (int16_t *) output_buf;
    *output_size = 176 * tx_interval_ms;

    memset(result_buf, 0, *output_size);

    for (size_t i = 0; i < n; ++i) {
        int16_t *input_data = (int16_t *) inputs[i].data;

        inputs[i].consumed = std::min((*output_size), inputs[i].len);
        size_t length = inputs[i].consumed / 2;

        for (size_t j = 0; j < length; ++j)
            result_buf[j] = add(result_buf[j], input_data[j]);
    }
}
