/*******************************************************************************
*                                                                              *
* server/ServerUDP.hpp                                                         *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#ifndef SERVER_SERVERUDP_HPP
#define SERVER_SERVERUDP_HPP

#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <list>
#include <vector>
#include "ServerClient.hpp"
#include "ServerSettings.hpp"

using boost::asio::ip::udp;
using boost::shared_ptr;

class ServerUDP
{
public:
    ServerUDP(boost::asio::io_service &ioService,
              std::map<uint32_t, ServerClient::pointer> *clients,
              const ServerSettings &settings);


private:
    void handleReceive(const boost::system::error_code &ec, size_t bytes);
    void retransmit(unsigned long datagramId);
    shared_ptr<std::string> mixerOutput();
    void saveDatagram(unsigned long id, shared_ptr<std::string> datagram);
    void send(shared_ptr<std::string> msg);
    void send(const udp::endpoint &endpoint, shared_ptr<std::string> msg);
    void sendAck(unsigned long datagramId, size_t win);
    void sendMixerOutput(shared_ptr<boost::asio::deadline_timer> dt,
                         const boost::system::error_code &ec);
    void startReceive();


    const ServerSettings &settings;
    std::map<uint32_t, ServerClient::pointer> *clients;
    std::map<udp::endpoint, ServerClient::weakPointer> clientEndpoints;

    udp::socket socket;
    udp::endpoint remote;
    boost::array<char, 65535> recvBuffer;

    unsigned long datagramCount = 0;
    std::list<std::pair<unsigned long, shared_ptr<std::string>>> datagramHistory;

};

#endif // SERVER_SERVERUDP_HPP
