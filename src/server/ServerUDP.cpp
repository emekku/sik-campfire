/*******************************************************************************
*                                                                              *
* server/ServerUDP.cpp                                                         *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#include "ServerUDP.hpp"

#include <boost/bind.hpp>
#include <iostream>
#include <sstream>
#include <utility>
#include "Mixer.hpp"

ServerUDP::ServerUDP(boost::asio::io_service &ioService,
                     std::map<uint32_t, ServerClient::pointer> *clients,
                     const ServerSettings &settings)
    : settings(settings),
      clients(clients),
      socket(ioService, udp::endpoint(udp::v6(), settings.port))
{
    startReceive();

    // Set the timer for periodic (every txInterval) mixer output dispatches.
    using boost::asio::deadline_timer;
    auto interval = boost::posix_time::millisec(0);
    shared_ptr<deadline_timer> dt(new deadline_timer(ioService, interval));
    dt->async_wait(boost::bind(&ServerUDP::sendMixerOutput, this, dt,
                               boost::asio::placeholders::error));
}

void ServerUDP::handleReceive(const boost::system::error_code &ec, size_t bytes)
{
    if (!ec || ec == boost::asio::error::message_size) {
        std::stringstream recvMsg(std::string(recvBuffer.data(), bytes));

        std::string cmd;
        recvMsg >> cmd;

        if (cmd == "CLIENT") // CLIENT
        {
            uint32_t clientId;
            recvMsg >> clientId;

            if (!clientId || !clients->count(clientId)) {
                std::cerr << "Invalid CLIENT ID in: " << recvMsg.str() << "\n";
            } else {
                ServerClient::pointer client = clients->at(clientId);

                if (!client->ready()) {
                    client->ready(true);
                    clientEndpoints[remote] = ServerClient::weakPointer(client);

                    std::cerr << "Client " << clientId << " joined (" << remote
                              << ").\n";
                }
            }
        }
        else if (cmd == "KEEPALIVE") // KEEPALIVE
        {
            // Nothing to do here.
        }
        else if (clientEndpoints.count(remote)) // Check if client is connected.
        {
            if (cmd == "UPLOAD") // UPLOAD
            {
                ServerClient::pointer client = clientEndpoints[remote].lock();

                if (client) {
                    uint32_t nr;
                    recvMsg >> nr;

                    if (nr == client->nextDatagramId()) {
                        shared_ptr<std::string> data(new std::string());
                        *data = recvMsg.str().substr(recvMsg.str().find('\n') + 1);

                        client->pushData(data);
                        client->nextDatagramId(nr + 1);
                        sendAck(nr, settings.fifoSize - client->fifoSize());
                    }
                }
            }
            else if (cmd == "RETRANSMIT") // RETRANSMIT
            {
                int id;
                recvMsg >> id;

                retransmit(id);
            }
        } else {
            std::cerr << "Server UDP received unknown message: "
                      << recvMsg.str() << "\n";
        }

        startReceive();
    } else {
        std::cerr << "Server UDP receive failed: " << ec.message() << "\n";
    }
}

void ServerUDP::retransmit(unsigned long datagramId)
{
    ServerClient::pointer client = clientEndpoints[remote].lock();
    if (!client)
        return;

    for (auto &it : datagramHistory) {
        auto datagram = it.second;

        if (it.first >= datagramId) {
            std::stringstream ss;
            ss << "DATA " << it.first << " " << client->nextDatagramId()
               << " " << client->win() << "\n" << *datagram;

            shared_ptr<std::string> msg(new std::string(ss.str()));

            send(msg);
        }
    }
}

void ServerUDP::saveDatagram(unsigned long id, shared_ptr<std::string> datagram)
{
    while (datagramHistory.size() >= settings.bufLen)
        datagramHistory.pop_front();

    datagramHistory.push_back(std::make_pair(id, datagram));
}

void ServerUDP::send(shared_ptr<std::string> msg)
{
    socket.async_send_to(boost::asio::buffer(*msg), remote,
        [this](boost::system::error_code ec, size_t /*bytes*/)
        {
            if (ec)
                std::cerr << "Server UDP send failed: " << ec.message() << "\n";
        });
}

void ServerUDP::send(const udp::endpoint &endpoint, shared_ptr<std::string> msg)
{
    socket.async_send_to(boost::asio::buffer(*msg), endpoint,
        [this](boost::system::error_code ec, size_t /*bytes*/)
        {
            if (ec)
                std::cerr << "Server UDP send failed: " << ec.message() << "\n";
        });
}

void ServerUDP::sendAck(unsigned long datagramId, size_t win)
{
    std::stringstream ss;
    ss << "ACK " << datagramId << " " << win << "\n";

    shared_ptr<std::string> msg(new std::string(ss.str()));
    send(msg);
}

void ServerUDP::sendMixerOutput(shared_ptr<boost::asio::deadline_timer> dt,
                                const boost::system::error_code &ec)
{
    dt->expires_at(dt->expires_at() +
                   boost::posix_time::millisec(settings.txInterval));
    dt->async_wait(boost::bind(&ServerUDP::sendMixerOutput, this, dt,
                               boost::asio::placeholders::error));

    if (ec) {
        std::cerr << "Server UDP mixer deadline timer failed: " << ec.message()
                  << "\n";
    }

    shared_ptr<std::string> mixerOutData = mixerOutput();

    for (auto &it : clientEndpoints) {
        auto &endpoint = it.first;
        auto client = it.second.lock();

        if (client) {
            std::stringstream ss;
            ss << "DATA " << datagramCount
               << " " << client->nextDatagramId()
               << " " << client->win()
               << "\n" << *mixerOutData;
            shared_ptr<std::string> msg(new std::string(ss.str()));

            send(endpoint, msg);
        }
    }

    saveDatagram(datagramCount, mixerOutData);
    datagramCount++;
}

shared_ptr<std::string> ServerUDP::mixerOutput()
{
    std::vector<mixer_input> mixerInput;
    std::vector<ServerClient::pointer> inputClients;

    for (auto &it : *clients) {
        auto client = it.second;

        if (client->ready() && client->fifoActive()) {
            inputClients.push_back(client);
            mixerInput.push_back(client->mixerInput());
        }
    }

    size_t outputSize = settings.dataLenFactor * settings.txInterval;
    char *mixerOutData = new char[outputSize];

    mixer(mixerInput.data(), mixerInput.size(), mixerOutData,
          &outputSize, settings.txInterval);

    shared_ptr<std::string> mixerOutput(
                new std::string(mixerOutData, outputSize));
    delete[] mixerOutData;

    for (size_t i = 0; i < inputClients.size(); ++i)
        inputClients[i]->removeData(mixerInput[i].consumed);

    return mixerOutput;
}

void ServerUDP::startReceive()
{
    socket.async_receive_from(boost::asio::buffer(recvBuffer), remote,
        boost::bind(&ServerUDP::handleReceive, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}
