/*******************************************************************************
*                                                                              *
* server/Server.hpp                                                            *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#ifndef SERVER_SERVER_HPP
#define SERVER_SERVER_HPP

#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <cstdint>
#include <map>
#include "ServerClient.hpp"
#include "ServerSettings.hpp"
#include "ServerTCP.hpp"
#include "ServerUDP.hpp"

using boost::asio::deadline_timer;
using boost::asio::ip::tcp;
using boost::asio::ip::udp;
using boost::shared_ptr;

class Server
{
public:
    Server(const ServerSettings &settings);

    void run();

private:
    boost::asio::io_service ioService;
    std::map<uint32_t, ServerClient::pointer> clients;

    const ServerSettings &settings;
    ServerTCP serverTCP;
    ServerUDP serverUDP;

};

#endif // SERVER_SERVER_HPP
