/*******************************************************************************
*                                                                              *
* server/ServerClient.hpp                                                      *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#ifndef SERVER_SERVERCLIENT_HPP
#define SERVER_SERVERCLIENT_HPP

#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <string>
#include "ServerSettings.hpp"
#include "Mixer.hpp"

using boost::asio::ip::tcp;
using boost::shared_ptr;

class ServerClient : public boost::enable_shared_from_this<ServerClient>
{
public:
    typedef boost::shared_ptr<ServerClient> pointer;
    typedef boost::weak_ptr<ServerClient> weakPointer;
    enum class FifoState { FILLING, ACTIVE };

    static pointer create(uint32_t id, const ServerSettings &settings,
                          boost::asio::io_service &ioService);

    bool connectionBroken() const;
    void connectionBroken(bool connectionBroken);
    bool fifoActive() const;
    size_t fifoMax() const;
    size_t fifoMin() const;
    size_t fifoSize() const;
    unsigned long id() const;
    unsigned long nextDatagramId();
    void nextDatagramId(unsigned long newNextDatagramId);
    mixer_input mixerInput();
    bool ready() const;
    void ready(bool ready);
    void resetFifoStats();
    tcp::socket & socket();
    size_t win() const;

    void start();

    void pushData(shared_ptr<std::string> data);
    void removeData(size_t len);

private:
    ServerClient(uint32_t id, const ServerSettings &settings,
                 boost::asio::io_service &ioService);

    unsigned long clientId;
    unsigned long nextDatagramId_ = 0;
    bool connectionBroken_ = false;
    bool clientReady = false;
    tcp::socket clientSocket;
    const ServerSettings &settings;

    std::string sendMessage;
    shared_ptr<char> fifo;
    char *fifoEnd;
    size_t fifoMaxBytes = 0;
    size_t fifoMinBytes = 0;
    FifoState fifoState;
};

#endif // SERVER_SERVERCLIENT_HPP
