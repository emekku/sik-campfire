/*******************************************************************************
*                                                                              *
* mainClient.cpp                                                               *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#include <csignal>
#include <getopt.h>
#include <iostream>
#include <vector>
#include "client/ClientInfo.hpp"
#include "client/ClientSettings.hpp"
#include "client/ClientTCP.hpp"
#include "client/ClientUDP.hpp"

using std::string;

void sigint_handler(int /*sig*/)
{
    std::cerr << "Terminating client.\n";
    exit(1);
}

int main(int argc, char** argv)
{
    std::string helpMsg("Usage: client -s <host> -p <port> "
                        "-X <retransmit_limit>.\n");
    // Override SIGINT handler
    if (signal(SIGINT, sigint_handler) == SIG_ERR) {
        std::cerr << "Failed to override SIGINT handler.\n";
        exit(errno);
    }

    ClientSettings *settings = new ClientSettings();

    int c;
    while ((c = getopt(argc, argv, "s:p:X:")) != -1) {
        switch (c) {
            case 's': // SERVER_NAME
                settings->host = optarg;
                break;
            case 'p': // PORT
                settings->port = optarg;
                break;
            case 'X': // RETRANSMIT_LIMIT
                settings->retransmitLimit = atoi(optarg);
                break;
            case '?':
                std::cout << helpMsg;

                return 0;
            default:
                break;
        }
    }

    if (settings->host.empty()) {
        std::cout << helpMsg;

        return -1;
    }

    try {
        boost::asio::io_service io;
        shared_ptr<const ClientSettings> clientSettings(settings);
        shared_ptr<ClientInfo> clientInfo(new ClientInfo());

        ClientTCP clientTCP(io, clientSettings, clientInfo);
        ClientUDP clientUDP(io, clientSettings, clientInfo);

        io.run();
    } catch (std::exception &e) {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}
