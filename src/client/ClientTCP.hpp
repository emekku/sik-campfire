/*******************************************************************************
*                                                                              *
* client/ClientTCP.hpp                                                         *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#ifndef CLIENT_CLIENTTCP_HPP
#define CLIENT_CLIENTTCP_HPP

#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <string>
#include "ClientInfo.hpp"
#include "ClientSettings.hpp"

using boost::asio::ip::tcp;
using boost::shared_ptr;

class ClientTCP
{
public:
    ClientTCP(boost::asio::io_service &io,
              shared_ptr<const ClientSettings> settings,
              shared_ptr<ClientInfo> clientInfo);

private:
    void connect();
    void handleReceive(const boost::system::error_code &ec, size_t bytes);
    void checkConnection(shared_ptr<boost::asio::deadline_timer> dt);
    void startReceive();

    shared_ptr<const ClientSettings> settings;
    shared_ptr<ClientInfo> clientInfo;

    tcp::socket socket;

    std::vector<char> recvBuffer;

    bool serverAlive = false;
};

#endif // CLIENT_CLIENTTCP_HPP
