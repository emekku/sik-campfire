/*******************************************************************************
*                                                                              *
* client/ClientInfo.hpp                                                        *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#include "ClientInfo.hpp"

#include <algorithm>

ClientInfo::ClientInfo()
{
    reset();
}

unsigned long ClientInfo::ack() const
{
    return ack_;
}

void ClientInfo::ack(unsigned long newAck)
{
    ack_ = newAck;
}

unsigned long ClientInfo::nextDatagramId() const
{
    return nextDatagramId_;
}

void ClientInfo::nextDatagramId(unsigned long newDatagramId)
{
    nextDatagramId_ = newDatagramId;
}

unsigned long ClientInfo::nrMaxSeen() const
{
    return nrMaxSeen_;
}

void ClientInfo::nrMaxSeen(unsigned long nr)
{
    nrMaxSeen_ = std::max(nr, nrMaxSeen_);
}

unsigned long ClientInfo::id() const
{
    return id_;
}

void ClientInfo::id(unsigned long newId)
{
    id_ = newId;
}

unsigned long ClientInfo::lastSentDatagram()
{
    return lastSent_;
}

void ClientInfo::lastSentDatagram(unsigned long datagramId)
{
    lastSent_ = datagramId;
}

ClientInfo::State ClientInfo::state() const
{
    return state_;
}

size_t ClientInfo::win() const
{
    return win_;
}

void ClientInfo::win(size_t newWin)
{
    win_ = newWin;
}

bool ClientInfo::awaitingData() const
{
    return ack() == nextDatagramId();
}

void ClientInfo::reset()
{
    ack_ = 0;
    id_ = 0;
    lastSent_ = 0;
    nextDatagramId_ = 0;
    nrMaxSeen_ = 0;
    state_ = State::NEW;
    win_ = 0;
}
