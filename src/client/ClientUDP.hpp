/*******************************************************************************
*                                                                              *
* client/ClientUDP.hpp                                                         *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#ifndef CLIENT_CLIENTUDP_HPP
#define CLIENT_CLIENTUDP_HPP

#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <queue>
#include "ClientInfo.hpp"
#include "ClientSettings.hpp"

using boost::asio::ip::udp;
using boost::shared_ptr;

class Client;

class ClientUDP
{
public:
    ClientUDP(boost::asio::io_service &io,
              shared_ptr<const ClientSettings> &settings,
              shared_ptr<ClientInfo> clientInfo);

private:
    void connect();
    void checkConnection(shared_ptr<boost::asio::deadline_timer> dt);
    void handleReceive(const boost::system::error_code &ec, size_t bytes);
    void keepAlive(shared_ptr<boost::asio::deadline_timer> dt);
    void processInput();
    void processOutput(shared_ptr<std::string> data);
    void resend();
    void retransmit();
    void send(shared_ptr<std::string> msg);
    void sendClientId();
    void startReceive();
    void writeToStdout();

    shared_ptr<const ClientSettings> settings;
    shared_ptr<ClientInfo> status;

    udp::socket socket;
    udp::endpoint remote;
    udp::endpoint recvEndpoint;

    bool serverAlive = false;
    bool readyToReceive = true;
    bool readyToWrite = true;
    bool readyToRead = true;
    boost::array<char, 65535> recvBuffer;
    std::vector<char> stdinBuffer;
    std::queue<shared_ptr<std::string>> stdoutQueue;
    std::string dataBuffer;
    std::string lastDataMsg;

    boost::asio::posix::stream_descriptor stdin;
    boost::asio::posix::stream_descriptor stdout;
};

#endif // CLIENT_CLIENTUDP_HPP
