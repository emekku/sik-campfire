/*******************************************************************************
*                                                                              *
* client/ClientSettings.hpp                                                    *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#ifndef CLIENT_CLIENTSETTINGS_HPP
#define CLIENT_CLIENTSETTINGS_HPP

class ClientSettings
{
public:
    std::string host;
    std::string port = "14685";
    unsigned long retransmitLimit = 10;

    size_t maxDataSize = 10560;
    unsigned long keepAliveFreq= 100;
    unsigned long recheckIdFreq = 100;
    unsigned long reconnectFreq = 500;
};

#endif // CLIENT_CLIENTSETTINGS_HPP
