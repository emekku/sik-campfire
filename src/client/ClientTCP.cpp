/*******************************************************************************
*                                                                              *
* client/ClientTCP.cpp                                                         *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#include "ClientTCP.hpp"

#include <boost/bind.hpp>
#include <iostream>
#include <sstream>

ClientTCP::ClientTCP(boost::asio::io_service &io,
                     shared_ptr<const ClientSettings> settings,
                     shared_ptr<ClientInfo> clientInfo)
 : settings(settings),
   clientInfo(clientInfo),
   socket(io),
   recvBuffer(settings->maxDataSize, 0)
{
    using boost::asio::deadline_timer;
    shared_ptr<deadline_timer> dt;
    auto interval = boost::posix_time::millisec(0);

    dt = shared_ptr<deadline_timer>(new deadline_timer(io, interval));
    checkConnection(dt);
}

void ClientTCP::connect()
{
    tcp::resolver tcpResolver(socket.get_io_service());
    auto endpointIt = tcpResolver.resolve({ settings->host, settings->port });

    boost::system::error_code ec;

    boost::asio::connect(socket, endpointIt, ec);
    socket.set_option(tcp::no_delay(true));

    if (ec) {
        std::cerr << "Client TCP connect failed: " << ec.message() << "\n";

        return;
    }

    serverAlive = true;

    startReceive();
}

void ClientTCP::handleReceive(const boost::system::error_code &ec,
                              size_t bytes)
{
    if (!ec || ec == boost::asio::error::message_size) {
        std::stringstream recvMsg(std::string(recvBuffer.data(), bytes));
        std::string cmd;

        recvMsg >> cmd;

        if (cmd == "CLIENT") {
            unsigned long clientId;
            recvMsg >> clientId;

            std::cerr << "Client's id: " << clientId << "\n";

            clientInfo->id(clientId);
        } else {
            std::cerr << recvMsg.str(); // REPORTS
        }

        startReceive();
    } else {
        // std::cerr << "Client TCP read failed: " << ec.message() << "\n";
        serverAlive = false;
    }

}

void ClientTCP::checkConnection(shared_ptr<boost::asio::deadline_timer> dt)
{
    dt->expires_at(dt->expires_at() +
                   boost::posix_time::millisec(settings->reconnectFreq));
    dt->async_wait(boost::bind(&ClientTCP::checkConnection, this, dt));

    if (!serverAlive) {
        clientInfo->reset();
        connect();
    }
}

void ClientTCP::startReceive()
{
    socket.async_read_some(boost::asio::buffer(recvBuffer),
        boost::bind(&ClientTCP::handleReceive, this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));
}
