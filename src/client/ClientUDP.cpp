/*******************************************************************************
*                                                                              *
* client/ClientUDP.cpp                                                         *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#include "ClientUDP.hpp"

#include <boost/asio/posix/stream_descriptor.hpp>
#include <boost/bind.hpp>

using boost::asio::posix::stream_descriptor;

ClientUDP::ClientUDP(boost::asio::io_service &io,
                     shared_ptr<const ClientSettings> &settings,
                     shared_ptr<ClientInfo> clientInfo)
    : settings(settings),
      status(clientInfo),
      socket(io),
      stdinBuffer(settings->maxDataSize, 0),
      dataBuffer(settings->maxDataSize, 0),
      stdin(io, dup(STDIN_FILENO)),
      stdout(io, dup(STDOUT_FILENO))
{
    using boost::asio::deadline_timer;
    auto interval = boost::posix_time::millisec(0);
    shared_ptr<deadline_timer> dt;

    dt = shared_ptr<deadline_timer>(new deadline_timer(io, interval));
    checkConnection(dt);
}

void ClientUDP::connect()
{
    if (!socket.is_open()) {
        udp::resolver udpResolver(socket.get_io_service());
        remote = *udpResolver.resolve({ settings->host, settings->port });
        socket.open(udp::v6());
    }

    sendClientId();
    startReceive();
}

void ClientUDP::checkConnection(shared_ptr<boost::asio::deadline_timer> dt)
{
    dt->expires_at(dt->expires_at() +
                   boost::posix_time::millisec(settings->reconnectFreq));
    dt->async_wait(boost::bind(&ClientUDP::checkConnection, this, dt));

    if (serverAlive) {
        serverAlive = false;
        return;
    }

    connect();
}

void ClientUDP::startReceive()
{
    if (!readyToReceive)
        return;
    readyToReceive = false;

    socket.async_receive_from(boost::asio::buffer(recvBuffer), recvEndpoint,
        boost::bind(&ClientUDP::handleReceive, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}

void ClientUDP::handleReceive(const boost::system::error_code &ec, size_t bytes)
{
    if (!ec || ec == boost::asio::error::message_size) {
        std::stringstream recvMsg(std::string(recvBuffer.data(), bytes));
        std::string cmd;

        recvMsg >> cmd;
        serverAlive = true;

        if (cmd == "DATA") // DATA
        {
            unsigned long nextDatagramId = status->nextDatagramId();
            unsigned long nr, ack, win;
            shared_ptr<std::string> data (new std::string());

            recvMsg >> nr >> ack >> win;
            *data = recvMsg.str().substr(recvMsg.str().find('\n') + 1);

            if (nr > nextDatagramId
                    && nr <= nextDatagramId + settings->retransmitLimit
                    && nextDatagramId > status->nrMaxSeen()) {
                retransmit();
            } else {
                processOutput(data);
            }

            status->nextDatagramId(nr + 1);
            status->nrMaxSeen(nr);
            status->win(win);
            status->ack(ack);

            if (ack != status->lastSentDatagram()) {
                resend(); // Resend last datagram.
            } else {
                readyToRead = true;
            }
        } else if (cmd == "ACK") // ACK
        {
            unsigned long ack, win;
            recvMsg >> ack >> win;

            status->ack(ack);
            status->win(win);
        }

        readyToReceive = true;
        processInput();
        startReceive();
    } else {
        std::cerr << "Client UDP receive failed: " << ec.message() << "\n";
    }
}

void ClientUDP::keepAlive(shared_ptr<boost::asio::deadline_timer> dt)
{
    dt->expires_at(dt->expires_at() +
                   boost::posix_time::millisec(settings->keepAliveFreq));
    dt->async_wait(boost::bind(&ClientUDP::keepAlive, this, dt));

    shared_ptr<std::string> msg(new std::string("KEEPALIVE\n"));
    send(msg);
}

void ClientUDP::processInput()
{
    if (status->win() <= 0 || !readyToRead || status->awaitingData())
        return;

    readyToRead = false;

    stdin.async_read_some(
        boost::asio::buffer(stdinBuffer.data(), status->win()),
        [this](boost::system::error_code ec, size_t bytes)
        {
            if (!ec) {
                lastDataMsg = std::string(stdinBuffer.data(), bytes);
                std::stringstream ss;
                ss << "UPLOAD " << status->ack() << "\n" << lastDataMsg;

                shared_ptr<std::string> msg(new std::string(ss.str()));
                send(msg);
                status->ack(status->ack() + 1);

                readyToRead = true;
            } else if (ec != boost::asio::error::eof) {
                std::cerr << "Client input process failed: " << ec.message()
                          << "\n";
            }
    });
}

void ClientUDP::processOutput(shared_ptr<std::string> data)
{
    stdoutQueue.push(data);

    if (!readyToWrite)
        return;
    readyToWrite = false;

    writeToStdout();
}

void ClientUDP::resend() {
    std::stringstream ss;
    ss << "UPLOAD " << status->lastSentDatagram() << "\n" << lastDataMsg;
    shared_ptr<std::string> msg(new std::string(ss.str()));

    send(msg);
}

void ClientUDP::retransmit()
{
    std::stringstream strstr;
    strstr << "RETRANSMIT " << status->nextDatagramId() << "\n";

    shared_ptr<std::string> msg(new std::string(strstr.str()));
    send(msg);
}

void ClientUDP::send(shared_ptr<std::string> msg)
{
    socket.async_send_to(boost::asio::buffer(*msg), remote,
        [this](boost::system::error_code ec, size_t /*bytes*/)
        {
            if (ec)
                std::cerr << "Client UDP send failed: " << ec.message() << "\n";
    });
}

void ClientUDP::sendClientId()
{
    if (!status->id())
        return;

    std::stringstream ss;
    ss << "CLIENT " << status->id() << "\n";
    shared_ptr<std::string> msg(new std::string(ss.str()));

    send(msg);

    using boost::asio::deadline_timer;
    auto &io = socket.get_io_service();
    auto interval = boost::posix_time::millisec(settings->keepAliveFreq);
    shared_ptr<deadline_timer> dt(new deadline_timer(io, interval));
    keepAlive(dt);
}

void ClientUDP::writeToStdout()
{
    shared_ptr<std::string> top = stdoutQueue.front();

    stdout.async_write_some(boost::asio::buffer(*top),
        [this](boost::system::error_code ec, size_t /*bytes*/)
        {
            if (ec) {
                std::cerr << "Client write to output failed: " << ec.message()
                          << "\n";
            } else {
                stdoutQueue.pop();

                if (stdoutQueue.size())
                    writeToStdout();
                else
                    readyToWrite = true;
            }
    });
}
