/*******************************************************************************
*                                                                              *
* client/ClientInfo.hpp                                                        *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#ifndef CLIENTINFO_HPP
#define CLIENTINFO_HPP

#include <cstdlib>

class ClientInfo
{
public:
    enum class State { NEW, CONNECTED };

    ClientInfo();

    unsigned long ack() const;
    void ack(unsigned long newAck);
    unsigned long id() const;
    void id(unsigned long newId);
    unsigned long lastSentDatagram();
    void lastSentDatagram(unsigned long datagramId);
    unsigned long nextDatagramId() const;
    void nextDatagramId(unsigned long newDatagramId);
    unsigned long nrMaxSeen() const;
    void nrMaxSeen(unsigned long nr);
    State state() const;
    size_t win() const;
    void win(size_t newWin);

    bool awaitingData() const;
    void reset();

private:
    unsigned long ack_;
    unsigned long id_;
    unsigned long lastSent_;
    unsigned long nextDatagramId_;
    unsigned long nrMaxSeen_;
    State state_;
    size_t win_;

};

#endif // CLIENTINFO_HPP
