/*******************************************************************************
*                                                                              *
* mainServer.cpp                                                               *
*                                                                              *
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
*                                                                              *
* Copyright (C) 2014 Przemysław Kuczyński, p.kuczynski [at] mimuw.edu.pl       *
*                                                                              *
*******************************************************************************/

#include <csignal>
#include <getopt.h>
#include <iostream>
#include "server/Server.hpp"
#include "server/ServerSettings.hpp"

void sigint_handler(int /*sig*/)
{
    std::cerr << "Terminating server.\n";
    exit(1);
}

int main(int argc, char** argv)
{
    // Override SIGINT handler
    if (signal(SIGINT, sigint_handler) == SIG_ERR) {
        std::cerr << "Failed to override SIGINT handler.\n";
        exit(errno);
    }

    ServerSettings serverSettings;
    int c;
    while ((c = getopt(argc, argv, "F:H:i:L:p:X:")) != -1) {
        switch (c) {
            case 'F': // FIFO_SIZE
                serverSettings.fifoSize = atoi(optarg);
                break;
            case 'H': // FIFO_HIGH_WATERMARK
                serverSettings.fifoHighWatermark = atoi(optarg);
                break;
            case 'i': // TX_INTERVAL
                serverSettings.txInterval = atoi(optarg);
                break;
            case 'L': // FIFO_LOW_WATERMARK
                serverSettings.fifoLowWatermark = atoi(optarg);
                break;
            case 'p': // PORT
                serverSettings.port = atoi(optarg);
                break;
            case 'X': // BUF_LEN
                serverSettings.bufLen = atoi(optarg);
                break;
            case '?':
                break;
            default:
                return -1;
        }
    }

    try {
        Server server(serverSettings);

        server.run();
    } catch (std::exception &e) {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}
